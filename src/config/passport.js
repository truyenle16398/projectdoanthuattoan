const LocalStrategy = require('passport-local').Strategy;

const User = require('../app/models/user');
const Job = require('../app/models/sv');
const Blog = require('../app/models/blog');
const Contact = require('../app/models/contact');
const About = require('../app/models/about');

module.exports = function (passport) {
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize user
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  // Signup
  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  },
    function (req, email, password, done) {//đăng ký
      User.findOne({ 'local.email': email }, function (err, user) {//kiểm tra có tốn tại email chưa
        if (err) {
          return done(err);
        }
        if (user) {
          return done(null, false, req.flash('signupMessage', 'the email is already taken'));// return email đã tồn tại
        } else {
          var newUser = new User();
          newUser.local.email = email;//kiểm tra email
          newUser.local.password = newUser.generateHash(password);
          newUser.save(function (err) {
            if (err) { throw err; }
            return done(null, newUser);
          });
        }
      });
    }));

  // login
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local
  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  },
    function (req, email, password, done) {//đăng nhập
      User.findOne({ 'local.email': email }, function (err, user) {
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, req.flash('loginMessage', 'No User found'))//không tìm thấy user
        }
        if (!user.validPassword(password)) {
          return done(null, false, req.flash('loginMessage', 'Wrong. password'));//pass sai
        }
        return done(null, user);
      });
    }));


}
