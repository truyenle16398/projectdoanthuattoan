const db = "mongodb://127.0.0.1:27017/DB"

const mongoose = require('mongoose');
const Job = mongoose.model('Job');
const Blog = mongoose.model('Blog');
mongoose.connect(db, err => {
	if (err) {
		console.error('error!' + err)
	}
	else {
		console.log('Connected to mongodb')
	}
})
module.exports = (app, passport) => {

	// index routes
	app.get('/', (req, res) => {
		Job.find((err, docs) => {
			if (!err) {
				res.render("employee/index", {
					danhsach: docs
				});
			}
			else {
				console.log('Error in retrieving job index :' + err);
			}
		});
	});

	//login view
	app.get('/login', (req, res) => {
		res.render('employee/login', {
			message: req.flash('loginMessage')
		});
	});

	app.post('/login', passport.authenticate('local-login', {
		successRedirect: 'admin',
		failureRedirect: '/login',
		failureFlash: true
	}));

	// signup view
	app.get('/signup', (req, res) => {
		res.render('employee/signup', {
			message: req.flash('signupMessage')
		});
	});

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect: 'employee/admin',
		failureRedirect: '/signup',
		failureFlash: true // allow flash messages
	}));
	//about admin
	app.get('/admin', isLoggedIn, (req, res) => {
		res.render('employee/admin', {
		});
	});

	// logout
	app.get('/logout', (req, res) => {
		req.logout();
		res.redirect('/');
	});

};

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect('/login');

}

