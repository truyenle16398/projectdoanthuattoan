const mongoose = require('mongoose');

var contactSchema = new mongoose.Schema({
    name: {
        type: String,
        required: 'This field is required.'
    },
    position:{
        type:String
    },
    response:{
        type:String
    }
});
mongoose.model('Contact', contactSchema);