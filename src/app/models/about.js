const mongoose = require('mongoose');

var aboutSchema = new mongoose.Schema({
    gioithieu: {
        type: String,
        required: 'This field is required.'
    },
    namthanhlap:{
        type:String
    },
    ungvien:{
        type:String
    },
    vieclam:{
        type:String
    },
    nhatuyendung:{
        type:String
    }
});
mongoose.model('About', aboutSchema);