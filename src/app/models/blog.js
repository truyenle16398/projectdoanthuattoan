const mongoose = require('mongoose');

var blogSchema = new mongoose.Schema({
    blogName: {
        type: String,
        required: 'This field is required.'
    },
    blogImages:{
        type:String
    },
    blogCount:{
        type:String
    }
});
mongoose.model('Blog', blogSchema);