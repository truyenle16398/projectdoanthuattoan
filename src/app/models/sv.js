const mongoose = require('mongoose');

var jobSchema = new mongoose.Schema({
    email : {
        type: String,
        required: 'This field is required.'
    },
    jobName: {
        type: String
    },
    address: {
        type: String
    },
    typeOfwork: {
        type: String
    },
    jobDescription: {
        type: String
    },
    companyName:{
        type: String
    },
    facebook:{
        type: String
    },
    images:{
        type:String
    }
});

// Custom validation for email
jobSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

mongoose.model('Job', jobSchema);