const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Job = mongoose.model('Job');
const Contact = mongoose.model('Contact');
const Blog = mongoose.model('Blog');
const About = mongoose.model('About');
const db = "mongodb://127.0.0.1:27017/DB"
mongoose.connect(db, err => {
	if (err) {
	  console.error('error!' + err)
	}
	else {
	  console.log('Connected to mongodb')
	}
  })
router.get('/', (req, res) => {
    Job.find((err, docs) => {
        if (!err) {
            res.render("employee/index", {
                danhsach:docs
            });
        }
        else {
            console.log('Error in retrieving job index :' + err);
        }
    });
});

router.post('/', (req, res) => {
    if (req.body._id == '')
        insertRecord(req, res);
    else
        updateRecord(req, res);
    if(req.body.jobName == '')
        search(req, res);
});
function insertRecord(req, res) {
    var job = new Job();
    job.email = req.body.email;
    job.jobName = req.body.jobName;
    job.address = req.body.address;
    job.typeOfwork = req.body.typeOfwork;
    job.jobDescription = req.body.jobDescription;
    job.companyName = req.body.companyName;
    job.facebook = req.body.facebook;
    job.save((err, doc) => {
        if (!err){
            res.redirect('employee/list');
        }      
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("employee/admin", {
                    viewTitle: "Insert Job Infomation",
                    job: req.body
                });
            }
            else
                console.log('Error during record insertion : ' + err);
        }
    });
    var contact= new Contact();
    contact.name = req.body.name;
    contact.position = req.body.position;
    contact.response = req.body.response;
    contact.save((err, doc) => {
      
    });
}

function updateRecord(req, res) {
    Job.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, doc) => {
        if (!err) { res.redirect('employee/list'); }
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("employee/admin", {
                    viewTitle: 'Update Job Infomation',
                    job: req.body
                });
            }
            else
                console.log('Error during record update : ' + err);
        }
    });
}

//chuyển hướng 
router.get('/list', (req, res) => {
    Job.find((err, docs) => {
        if (!err) {
            res.render("employee/list", {
                list: docs
            });
        }
        else {
            console.log('Error in retrieving job list :' + err);
        }
    });
});
//index
router.get('/index', (req, res) => {
    Job.find((err, docs) => {
        if (!err) {
           res.render("employee/index", {
                danhsach:docs
            });
        }       else {
            console.log('Error in retrieving job index :' + err);
       }
   });
});

//contact
router.get('/contact', (req, res) => {
    Contact.find((err, docs) => {
        if (!err) {
            res.render("employee/contact", {
                ds1: docs
            });
        }
        else {
            console.log('Error in retrieving job contact :' + err);
        }
    });
});
//login
router.get('/login', (req, res) => {
    Job.find((err, docs) => {
        if (!err) {
            res.render("employee/login", {
                
            });
        }
        else {
            console.log('Error in retrieving job login :' + err);
        }
    });
});
router.get('/signup', (req, res) => {
    Job.find((err, docs) => {
        if (!err) {
            res.render("employee/signup", {
                
            });
        }
        else {
            console.log('Error in retrieving job signup :' + err);
        }
    });
});
//blog
router.get('/blog', (req, res) => {
    Blog.find((err, docs) => {
        if (!err) {
            res.render("employee/blog", {
                ds: docs
            });
        }
        else {
            console.log('Error in retrieving job blog :' + err);
        }
    });
});

//about
router.get('/about', (req, res) => {
    About.find((err, docs) => {
        if (!err) {
            res.render("employee/about", {
                ds2: docs
            });
        }
        else {
            console.log('Error in retrieving job about :' + err);
        }
    });
});
router.get('/admin', (req, res) => {
    Job.find((err, docs) => {
        if (!err) {
            res.render("employee/admin", {
                
            });
        }
        else {
            console.log('Error in retrieving job admin :' + err);
        }
    });
});
function handleValidationError(err, body) {
    for (field in err.errors) {
        switch (err.errors[field].path) {
            case 'email':
                body['emailError'] = err.errors[field].message;
                break;
            default:
                break;
        }
    }
}

router.get('/:id', (req, res) => {
    Job.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.render("employee/admin", {
                viewTitle: "Update Job Infomation",
                job: doc
            });
        }
    });
});


router.get('search/:jobName',(req, res)=>{
    var chuoi=req.params.jobName
    Job.find({jobName: new RegExp(chuoi, "i")}, function(err, job){
      if(err){
        console.log(err)
      }
      else{
        console.log(job)
      }
    })
  })

router.get('/delete/:id', (req, res) => {
    Job.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.redirect('/employee/list');
        }
        else { console.log('Error in job delete :' + err); }
    });
});

module.exports = router;

